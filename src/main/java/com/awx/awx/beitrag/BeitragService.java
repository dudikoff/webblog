package com.awx.awx.beitrag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class BeitragService {

    @Autowired
    private BeitragRepository beitragRepository;

    public void initData() {

        Beitrag beitrag1 = new Beitrag();
        beitrag1.setCreationDate(Instant.now());
        beitrag1.setText("Antike Außerirdische Astronauten, die die Erde besucht haben, sollen mit ihren fortgeschrittenen Technologien unmöglich scheinende Monumentalbauwerke geschaffen \n" +
                "und diese Geschehnisse dann zu Göttergeschichten verklärt wurden.\n" +
                "So wurden die Pyramiden von Gizeh vor über 4000 Jahren mathematisch perfekt ausgerichtet zu den Sternen des Kosmos erbaut.\n" +
                "Fakt ist das es bis heute keine Informationen über die Planung, den Hilfsmitteln und Gerätschaften, sowie der Erbauung gibt.\n" +
                "Wissenschaftliche Untersuchungen haben ergeben das der Bau der Pyramiden nicht mehr als 20 Jahre gedauert hat!\n" +
                "Um das zu schaffen wären 431 tonnenschwere Steinblöcke täglich auf die exakt ermittelte Position nötig gewesen!\n" +
                "Die Baumethoden und das Ingenieurswissen wurde erst 1000 Jahre danach entwickelt.\n" +
                "\n" +
                "Der Autor Erich von Däniken ist überzeugt das hier Aliens mit im Spiel waren und ebenso das diese eines Tages zurückkehren werden…\n" +
                "Ob uns die Aliens wieder freundlich gesinnt sein werden bleibt abzuwarten, Stephen Hawking hat hierzu folgendes Zitat verlautet:\n" +
                "\n" +
                "Wenn Aliens uns jemals besuchen sollten, denke ich, ist das Ergebnis so wie bei Christopher Kolumbus und seiner ersten \n" +
                "Ankunft in Amerika – was nicht sonderlich gut für die amerikanischen Ureinwohner ausgegangen ist.\n");
        beitrag1.setTitle("Haben Aliens die Pyramiden im Alten Ägypten gebaut?\n");
//        beitrag1.setUser(new user("Jesse", "Lange"));  //user kann bis Dato leider nicht gesetzt werden
        beitragRepository.save(beitrag1);

    }

    public List<Beitrag> leseBeitraegeZurUeberschrift(String title) {
        List<Beitrag> beitraege = beitragRepository.findByTitle(title);
        return beitraege;
    }


}
