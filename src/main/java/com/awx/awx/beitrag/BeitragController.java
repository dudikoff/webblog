package com.awx.awx.beitrag;

import com.awx.awx.kommentar.KommentarDTO;
import com.awx.awx.kommentar.KommentarRepository;
import com.awx.awx.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class BeitragController {

    @Autowired
    private BeitragRepository beitragRepository;
    @Autowired
    KommentarRepository kommentarRepository;
    @Autowired
    BeitragService beitragService;


    //create Beitreag
    @GetMapping("/beitrag")
    public String createBeitrag(Model model) {
        model.addAttribute("beitrag", new BeitragDTO());
        return "create_beitrag";
    }


    @PostMapping("/beitrag")
    public String createBeitrag(@ModelAttribute("beitrag") @Valid BeitragDTO beitragDTO, BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser) {
        if (bindingResult.hasErrors()) {
            return "create_beitrag";
        }
        Beitrag beitrag = new Beitrag(beitragDTO.getText(), beitragDTO.getTitle(), currentUser);
        beitragRepository.save(beitrag);
        return "redirect:/";
    }


    @GetMapping("/details")
    public String viewBeitrag(Model model, @RequestParam("beitragId") long id) {
        Beitrag beitrag = beitragRepository.findById(id).get();
        model.addAttribute("kommentarList", beitrag.getKommentarList());

        model.addAttribute("beitragId", beitrag.getId());
        BeitragDTO beitragDTO = new BeitragDTO(beitrag.getText(), beitrag.getTitle(), beitrag.getCreationDate(), beitrag.getUser());
        model.addAttribute("beitrag", beitragDTO);
        KommentarDTO kommentarDTO = new KommentarDTO();
        model.addAttribute("kommentarDTO", kommentarDTO);
        return "beitragAnsicht";

    }


    @GetMapping("/beitragBearbeiten")
    public String beitragBearbeiten(Model model, @RequestParam("beitragId") long id) {
        Beitrag beitrag = beitragRepository.findById(id).get();
        model.addAttribute("beitragId", beitrag.getId());
        BeitragDTO beitragDTO = new BeitragDTO(beitrag.getText(), beitrag.getTitle(), beitrag.getCreationDate(), beitrag.getUser());
        model.addAttribute("beitrag", beitragDTO);
        return "beitragBearbeiten";
    }


    @PostMapping("/beitragBearbeiten")
    public String beitragBearbeiten(@RequestParam("beitragId") long id, @ModelAttribute("beitrag") BeitragDTO beitragDTO, BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser) {
        if (bindingResult.hasErrors()) {
            return "beitragBearbeiten";
        }
        Beitrag beitrag = beitragRepository.findById(id).get();
        beitrag.setTitle(beitragDTO.getTitle());
        beitrag.setText(beitragDTO.getText());
        beitragRepository.save(beitrag);
        return "redirect:/details?beitragId="+id;
    }


    @PostMapping("/beitragLoeschen")
    public String beitragLoeschen(@RequestParam("beitragId") Beitrag beitrag) {
        beitragRepository.deleteById(beitrag.getId());
        return "redirect:/";
    }


}
