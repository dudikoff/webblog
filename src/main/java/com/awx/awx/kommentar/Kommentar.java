package com.awx.awx.kommentar;


import com.awx.awx.beitrag.Beitrag;
import com.awx.awx.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Kommentar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Instant creationDate;

    private String kommentarTitle;

    @ManyToOne
    private User user;

    @ManyToOne
    private Beitrag beitrag;

    public Kommentar() {
    }

    public Kommentar( String kommentarTitle, Instant creationDate, User user, Beitrag beitrag) {
        this.creationDate = Instant.now();
        this.kommentarTitle = kommentarTitle;
        this.user = user;
        this.beitrag = beitrag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getKommentarTitle() {
        return kommentarTitle;
    }

    public void setKommentarTitle(String kommentarTitle) {
        this.kommentarTitle = kommentarTitle;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }
}
