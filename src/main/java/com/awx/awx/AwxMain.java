package com.awx.awx;

import com.awx.awx.beitrag.BeitragRepository;

import com.awx.awx.beitrag.BeitragService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwxMain {

	@Autowired
	BeitragService beitragService;
	@Autowired
	BeitragRepository beitragRepository;

	public static void main(String[] args) {
		SpringApplication.run(AwxMain.class, args);
	}

//	@PostConstruct
//	public void initData() {
//		beitragService.initData();
//	}
//		@PostConstruct
//		public void sortiereBeitraege(){
//
//		List<beitrag> beitragList = beitragRepository.findAllByOrderByIdDesc(); //o. auch beitragController
//		for (beitrag beitrag : beitragList) {
//			System.out.println(beitrag.getCreationDate() + " "+ beitrag.getTitle());
//		}
//	}


}
