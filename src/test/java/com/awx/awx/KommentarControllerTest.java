package com.awx.awx;

import com.awx.awx.beitrag.Beitrag;
import com.awx.awx.beitrag.BeitragRepository;
import com.awx.awx.beitrag.BeitragService;
import com.awx.awx.kommentar.KommentarDTO;
import com.awx.awx.user.User;
 import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.persistence.Id;
import java.util.Optional;

public class KommentarControllerTest {


    @Mock
    private BeitragRepository beitragRepository;

    @InjectMocks
    private BeitragService beitragService;

    @Before
    public void mocksVorbereiten(){
        MockitoAnnotations.initMocks(this);

    }

    @Test
     public void findById(){

        Optional<Beitrag> beitrag = Optional.of(new Beitrag());

        Mockito.when(beitragRepository.findById((long) 1)).thenReturn(beitrag);


        // Meine Implementierung testen
        Model model = Mockito.mock(Model.class);
        Mockito.doNothing().when(model).addAttribute(Mockito.anyString(), Mockito.anyLong());

        KommentarDTO kommentarDTO = new KommentarDTO();
        User user = Mockito.mock(User.class);
        Id id = Mockito.mock(Id.class);
        BindingResult bindingResult = Mockito.mock(BindingResult.class);
        beitragService.leseBeitraegeZurUeberschrift("");
     }
}
